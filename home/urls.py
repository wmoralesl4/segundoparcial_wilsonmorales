from django.urls import path, include
from . import views
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    
    path('', views.HomeTemplateView.as_view(), name='home'),
    path('cliente/', views.lista.as_view(), name='lista'),
    path('cliente/<int:pk>/', views.detalle.as_view(), name='detalle'),
    path('cliente/crear/', views.crear.as_view(), name='crear'),
    path('cliente/<int:pk>/actualizar/', views.actualizar.as_view(), name='actualizar'),
    path('cliente/<int:pk>/eliminar/', views.eliminar.as_view(), name='eliminar'),
    
    path('login/', LoginView.as_view(template_name='login.html', next_page='home'), name='login'),
    path('logout/', LogoutView.as_view(next_page='login'), name='logout'),
    path('register/', views.RegisterView.as_view(), name='register'),

    path('api/', include('home.urlsAPI')),

    
]
