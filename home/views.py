from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from django.urls import reverse_lazy
from .models import cliente
from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

class lista(ListView):
    model = cliente
    template_name = "cliente/lista.html"
    context_object_name='clientes'
    paginate_by = 10
    def get_queryset(self):
        query = self.request.GET.get('clientes')
        if query:
            return cliente.objects.filter(Q(nombre__icontains=query)) 
        else:
            return cliente.objects.all()

class HomeTemplateView(TemplateView):
    template_name = 'home.html'

class RegisterView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('lista')
    template_name = 'registro.html'

    def form_valid(self, form):
        response = super().form_valid(form)
        auth_login(self.request, self.object)
        return response

class detalle(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = 'home.view_cliente'
    model = cliente
    template_name = 'cliente/detalle.html'
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('lista'))


class crear(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'home.add_cliente'
    model = cliente
    template_name = 'cliente/crear.html'
    fields = ['dni', 'nombre', 'direccion', 'fnacimiento']
    success_url = reverse_lazy('lista')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('lista'))

class actualizar(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'home.change_cliente'
    model = cliente
    template_name = 'cliente/actualizar.html'
    fields = ['dni', 'nombre', 'direccion', 'fnacimiento']
    success_url = reverse_lazy('lista')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('lista'))

class eliminar(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    permission_required = 'home.delete_cliente'
    model = cliente
    template_name = 'cliente/eliminar.html'
    success_url = reverse_lazy('lista')
    def handle_no_permission(self):
        return HttpResponseRedirect(reverse_lazy('lista'))
    