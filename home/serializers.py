from rest_framework import serializers
from .models import cliente
class Serializer(serializers.ModelSerializer):
    class Meta:
        model = cliente
        fields = '__all__'