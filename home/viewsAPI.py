from rest_framework import viewsets, permissions
from .serializers import serializers
from .models import *
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

class ClienteViewSet(viewsets.ModelViewSet):
    queryset = cliente.objects.all()
    serializer_class = serializers
    permission_classes = [permissions.IsAuthenticated]  
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated]
